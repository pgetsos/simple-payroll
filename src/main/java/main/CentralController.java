package main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CentralController implements Initializable {
    @FXML private TableView<Employer> employerTableView;
    @FXML private ComboBox months;
    @FXML private ComboBox years;
    @FXML private GridPane mainGrid;
    @FXML private  Text saveText;
    @FXML private  TextField addName;
    @FXML private  TextField addSurname;
    @FXML private  TextField addFixed;
    @FXML private  TextField addRate;
    @FXML private ToggleGroup toggleGroup;
    @FXML private Button buttonAdd;
    @FXML private VBox vboxEmployeesEdit;
    @FXML private Button saveButton;
    @FXML private PasswordField passwordField;
    @FXML private TextField usernameField;

    private final ObservableMap<Integer, Employer> dataInit = FXCollections.observableHashMap();
    private Connection conn;
    private Main application;
    private int maxID;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setDateSpinners();
        setEmployers();
        System.out.println(months.getSelectionModel().getSelectedItem());
        System.out.print(years.getSelectionModel().getSelectedItem());
    }

    private void setDateSpinners(){
        months.getItems().clear();
        years.getItems().clear();
        ObservableList monthList = FXCollections.observableList(SmallHelpers.getMonths());
        ObservableList yearsList = FXCollections.observableList(SmallHelpers.getYears());
        months.setItems(monthList);
        years.setItems(yearsList);
        months.getSelectionModel().select(SmallHelpers.getCurrentMonth());
        years.setValue(SmallHelpers.getCurrentYear());
    }

    private void setEmployers(){
        TextField newField = new TextField();
       // mainGrid.getChildren().add(newField);
    }

    public void handleSubmitButtonAction(ActionEvent actionEvent) {
        //re
    }

    private void talkToDBPay(){
        Statement stmt = null;
        ResultSet rs = null;
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT id, week, due, hours, week, week_date, paid FROM payroll, week_months WHERE " +
                    "payroll.month = '"+months.getSelectionModel().getSelectedItem()+"' AND payroll.year = '"+ years.getSelectionModel().getSelectedItem() +"'" +
                    " AND payroll.month_id = week_months.month_id");
            if(!rs.isBeforeFirst()){
                initDate();
            }
            while (rs.next()) {
                Employer em = new Employer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5));
                dataInit.put(em.getId(), em);
            }
            maxID = dataInit.size();
            employerTableView.getItems().addAll(dataInit.values());
        } catch (SQLException e){
            Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
        } finally {
            if(stmt != null){
                try {
                    stmt.close();
                }  catch (SQLException e){
                    Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
                }
            }
            if(rs != null){
                try {
                    rs.close();
                }  catch (SQLException e){
                    Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
                }
            }
        }
    }

    private void talkToDBEdit(){
        Statement stmt = null;
        ResultSet rs = null;
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT id, name, surname, fixed, rate FROM employees");
            while (rs.next()) {
                Employer em = new Employer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getInt(5));
                dataInit.put(em.getId(), em);
            }
            maxID = dataInit.size();
            employerTableView.getItems().addAll(dataInit.values());
        } catch (SQLException e){
            Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
        } finally {
            if(stmt != null){
                try {
                    stmt.close();
                }  catch (SQLException e){
                    Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
                }
            }
            if(rs != null){
                try {
                    rs.close();
                }  catch (SQLException e){
                    Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
                }
            }
        }
    }

    private void talkToDBSave(){
        PreparedStatement stmt = null;
        try{
            stmt = conn.prepareStatement("MERGE INTO employees KEY(id) VALUES (?, ?, ?, ?, ?)");
            for (Employer employer : employerTableView.getItems()) {
                stmt.setInt(1, employer.getId());
                stmt.setString(2, employer.getName());
                stmt.setString(3, employer.getSurname());
                stmt.setInt(4, employer.getFixed());
                stmt.setInt(5, employer.getRate());
                stmt.addBatch();
            }
            stmt.execute();

        } catch (SQLException e){
            Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
        } finally {
            if(stmt != null){
                try {
                    stmt.close();
                }  catch (SQLException e){
                    Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
                }
            }
        }
    }

    private void initDate(){

    }

    public void setApp(Main application, Connection conn){
        this.application = application;
        this.conn = conn;
        talkToDBEdit();
    }

    public void handleAddButtonAction(ActionEvent actionEvent) {
        HBox newBox = new HBox(5);
        TextField name = new TextField();
        TextField surname = new TextField();
        TextField fixed = new TextField();
        TextField rate = new TextField();
        name.setId("name");
        surname.setId("surname");
        fixed.setId("fixed");
        rate.setId("rate");
        name.setPrefWidth(150);
        surname.setPrefWidth(150);
        fixed.setPrefWidth(150);
        rate.setPrefWidth(150);
        newBox.getChildren().add(name);
        newBox.getChildren().add(surname);
        newBox.getChildren().add(fixed);
        newBox.getChildren().add(rate);
        //new GridPane().
    }

    public void handleSaveButtonAction(ActionEvent actionEvent) {
        talkToDBSave();
        saveText.setText("Αποθηκεύτηκε!");
    }

    public void addEmployer(ActionEvent actionEvent) {
        ObservableList<Employer> data = employerTableView.getItems();
        data.add(new Employer(maxID,
                addName.getText(),
                addSurname.getText(),
                Integer.parseInt(addFixed.getText()),
                Integer.parseInt(addRate.getText())
        ));
        maxID++;
        talkToDBSave();
        addName.setText("");
        addSurname.setText("");
        addFixed.setText("");
        addRate.setText("");
    }
}
