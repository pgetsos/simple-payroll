package main;

public class Employer {
    private String name;
    private String surname;
    private String fullName;
    private int fixed;
    private int rate;
    private int id;

    public Employer(int id, String name, String surname, int fixed, int rate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.fixed = fixed;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getFixed() {
        return fixed;
    }

    public void setFixed(int fixed) {
        this.fixed = fixed;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return name + " " + surname;
    }
}
