package main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;

import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController implements Initializable {
    @FXML private PasswordField passwordField;
    @FXML private TextField usernameField;
    @FXML private Text actionTarget;

    private Connection conn;
    private Main application;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        passwordField.setOnKeyPressed(event -> { if(event.getCode() == KeyCode.ENTER) { talkToDB(); } });
    }

    public void handleSubmitButtonAction(ActionEvent actionEvent) {
        actionTarget.setText("Sign in button pressed - Pass: "+passwordField.getText());
        talkToDB();
    }

    private void talkToDB(){
        Statement stm = null;
        try{
            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection("jdbc:h2:~/pgetsos/SimplePayroll", "anthoula", "panionios"/*usernameField.getText(), passwordField.getText()*/);
            // add application code here
            stm = conn.createStatement();
            stm.executeUpdate("CREATE TABLE IF NOT EXISTS employees\n" +
                    "(\n" +
                    "    id INT,\n" +
                    "    name VARCHAR(20),\n" +
                    "    surname VARCHAR(25),\n" +
                    "    fixed INT NOT NULL,\n" +
                    "    rate INT,\n" +
                    "    CONSTRAINT employees_id_pk PRIMARY KEY (id)\n" +
                    ");");
            stm.executeUpdate("CREATE TABLE IF NOT EXISTS payroll\n" +
                    "(\n" +
                    "    id INT,\n" +
                    "    month_id INT,\n" +
                    "    month INT,\n" +
                    "    year INT,\n" +
                    "    due INT,\n" +
                    "    hours INT,\n" +
                    "    CONSTRAINT payroll_id_pk PRIMARY KEY (id, month_id)\n" +
                    ");");
            stm.executeUpdate("CREATE TABLE IF NOT EXISTS week_months\n" +
                    "(\n" +
                    "    month_id INT,\n" +
                    "    week INT,\n" +
                    "    week_date VARCHAR(15),\n" +
                    "    paid INT,\n" +
                    "    CONSTRAINT week_id_pk PRIMARY KEY (month_id, week)\n" +
                    ");");
            goToCentral();
        } catch (org.h2.jdbc.JdbcSQLException e){
            if(e.getOriginalMessage().contains("Wrong user name or password")){
                actionTarget.setText("Wrong username or password!");
            }
            Logger.getLogger("d").log(Level.SEVERE,e.getOriginalMessage(),e);
        } catch (SQLException | ClassNotFoundException e){
            Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
        } finally {
            if(stm != null){
                try {
                    stm.close();
                }  catch (SQLException e){
                    Logger.getLogger("d").log(Level.SEVERE,"ex2",e);
                }
            }
        }
    }

    public void setApp(Main application){
        this.application = application;
    }

    private void goToCentral(){
        application.goToCentral(conn);
    }
}
