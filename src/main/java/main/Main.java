package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.InputStream;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {
    private final double MINIMUM_WINDOW_WIDTH = 390.0;
    private final double MINIMUM_WINDOW_HEIGHT = 500.0;
    private Parent root;
    private Stage stage;

    @Override
    public void start(Stage stage) throws Exception{
        this.stage = stage;
        stage.setTitle("Simple Payroll - pgetsos");
        gotoLogin();
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void gotoLogin() {
        try {
            LoginController login = (LoginController) replaceSceneContent("login.fxml");
            login.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void goToCentral(Connection conn){
        try {
            CentralController central = (CentralController) replaceSceneContent("central.fxml");
            central.setApp(this, conn);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Initializable replaceSceneContent(String fxml) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(fxml));


        root = loader.load();
        stage.setScene(new Scene(root, 800, 675));
        return (Initializable) loader.getController();
    }

}
