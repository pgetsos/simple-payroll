package main;

/**
 * Created by pgetsos on 4/2/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class MonthPay {

    private int id;
    private int monthId;
    private int month;
    private int year;
    private int due;
    private int hours;

    public MonthPay(int id, int monthId, int month, int year, int due, int hours) {
        this.id = id;
        this.monthId = monthId;
        this.month = month;
        this.year = year;
        this.due = due;
        this.hours = hours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getDue() {
        return due;
    }

    public void setDue(int due) {
        this.due = due;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
}
