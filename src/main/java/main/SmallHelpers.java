package main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by pgetsos on 4/1/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class SmallHelpers {
    private final static Calendar cal = Calendar.getInstance();

    public static List<String> getMonths() {
        List<String> months = new ArrayList<>();
        months.add("Ιανουάριος");
        months.add("Φεβρουάριος");
        months.add("Μάρτιοσ");
        months.add("Απρίλιος");
        months.add("Μάιος");
        months.add("Ιούνιος");
        months.add("Ιούλιος");
        months.add("Αύγουστος");
        months.add("Σεπτέμβριος");
        months.add("Οκτώβριος");
        months.add("Νοέμβριος");
        months.add("Δεκέμβριος");

        return months;
    }

    public static List<Integer> getYears() {
        List<Integer> years = new ArrayList<>();
        int current = cal.get(Calendar.YEAR);
        for (int i = 2015; i < current; i++) {
            years.add(i);
        }
        years.add(current);
        years.add(current+1);

        return years;
    }

    static int getCurrentMonth(){
        return cal.get(Calendar.MONTH);
    }

    static int getCurrentYear(){
        return cal.get(Calendar.YEAR);
    }
}
