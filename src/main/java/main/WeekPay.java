package main;

/**
 * Created by pgetsos on 4/2/2017. Γκετσόπουλος Πέτρος 3130041
 */
public class WeekPay {
    private int monthId;
    private int week;
    private int paid;
    private String weekDate;

    public WeekPay(int monthId, int week, int paid, String weekDate) {
        this.monthId = monthId;
        this.week = week;
        this.paid = paid;
        this.weekDate = weekDate;
    }

    public int getMonthId() {
        return monthId;
    }

    public void setMonthId(int monthId) {
        this.monthId = monthId;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getPaid() {
        return paid;
    }

    public void setPaid(int paid) {
        this.paid = paid;
    }

    public String getWeekDate() {
        return weekDate;
    }

    public void setWeekDate(String weekDate) {
        this.weekDate = weekDate;
    }
}
